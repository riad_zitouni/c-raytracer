**Summary**

This is my C++ raytracer project. This raytracer calculates shadows plus both reflected and refracted light and currently performs 6 light bounces.

**How to use this app?**

* Run the executable (in folder X64/Release) or run the project from Visual Studio
* After a few seconds a window will display a ray-traced rendering of planes and spheres