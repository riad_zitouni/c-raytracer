#ifndef RAYTRACER_H
#define RAYTRACER_H

#include "Material.h"
#include "Ray.h"
#include "Object.h"
#include "PointLight.h"

#include <vector>
#include <memory>

#define EPSILON 0.01f

namespace rt
{
	class Raytracer
	{
	public:
		void trace(const Ray& ray, const std::vector<std::shared_ptr<Object>>& objects, 
			const std::vector<PointLight>& lights, float indexOfRefraction, int depth, glm::vec3& color, float& t);
	private:
	};
}

#endif