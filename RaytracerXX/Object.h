#ifndef OBJECT_H
#define OBJECT_H

#include <math.h>
#include "Ray.h"
#include "Material.h"

class Object
{
public:
	Object();

	virtual const Material& getMaterial() const { return Material(); }

	virtual Ray::IntersectionType intersect(Ray ray, float& t)
	{
		return Ray::IntersectionType::MISS;
	}

	virtual glm::vec3 getNormalAt(glm::vec3 point)
	{
		return glm::vec3(0, 0, 0);
	}
};

#endif