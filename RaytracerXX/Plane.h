#ifndef Plane_H
#define Plane_H

#include <math.h>
#include "Material.h"
#include <math.h>
#include "Object.h"

namespace rt
{
	class Plane : public Object
	{
	public:
		Plane();
		Plane(glm::vec3 origin, glm::vec3 normal, const Material& Material);

		inline const glm::vec3& getPlaneNormal() const { return normal; }
		inline const Material& getMaterial() const override { return material; }

		glm::vec3 getNormalAt(glm::vec3 point) override;

		Ray::IntersectionType intersect(Ray ray, float& t) override;

	private:

		glm::vec3 origin;

		glm::vec3 normal;

		Material material;
	};
}
#endif

