#ifndef Camera_H
#define Camera_H

#include <math.h>

#include <glm\glm.hpp>

#define WORLD_UP glm::vec3(0,1,0)

namespace rt
{
	class Camera
	{
	public:
		Camera();
		Camera(glm::vec3 position, glm::vec3 lookAt);
		Camera(glm::vec3 position, glm::vec3 direction, glm::vec3 right, glm::vec3 up);

		inline const glm::vec3& getPosition() const{ return m_position; }
		inline const glm::vec3& getDirection() const{ return m_direction; }
		inline const glm::vec3& getRight() const{ return m_right; }
		inline const glm::vec3& getUp() const{ return m_up; }
		
		inline const glm::mat4& getCameraToWorldMatrix() const { return m_cameraToWorldMatrix; }


	private:
		void setCameraMatrix();

		glm::vec3 m_position;
	    glm::vec3 m_direction;
		glm::vec3 m_right;
		glm::vec3 m_up;

		glm::mat4 m_cameraToWorldMatrix;
	};
}

#endif
