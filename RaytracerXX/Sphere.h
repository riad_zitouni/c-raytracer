#pragma once

#ifndef SPHERE_H
#define SPHERE_H

#pragma once

#include "Object.h"

class Sphere : public Object
{
public:
	Sphere();
	Sphere(glm::vec3 origin, double radius, const Material& Material);

	inline const glm::vec3& getOrigin() const{ return m_origin; }
	inline const double& getRadius() const{ return m_radius; }
	
	const Material& getMaterial() const override{ return m_material; }

	Ray::IntersectionType intersect(Ray ray, float& t) override;

	glm::vec3 getNormalAt(glm::vec3 point) override;

private:

	glm::vec3 m_origin;
	double m_radius;
	Material m_material;
};
#endif
