#pragma once

#ifndef RAY_H
#define RAY_H

#include <math.h>
#include <glm\glm.hpp>

class Ray
{
public:
	enum IntersectionType : int
	{
		INTERNAL = -1,
		MISS = 0, 
		HIT = 1
	};

	Ray();
	Ray(glm::vec3 origin, glm::vec3 direction);

	inline const glm::vec3& getOrigin() const { return m_origin; }
	inline const glm::vec3& getDirection() const { return m_direction; }

private:

	glm::vec3 m_origin;
	glm::vec3 m_direction;
};
#endif
