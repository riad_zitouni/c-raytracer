#include "PointLight.h"

namespace rt
{
	PointLight::PointLight()
	{
		m_position = glm::vec3(0);
		m_material = Material(glm::vec4(1.0f, 1.0f, 1.0f, 0.0f));
	}

	PointLight::PointLight(glm::vec3 position, const Material& material)
	{
		this->m_position = position;
		this->m_material = material;
	}
}