#include "Raytracer.h"
#include "Math.h"
#include <iostream>

namespace rt
{
	void Raytracer::trace(const Ray& ray, const std::vector<std::shared_ptr<Object>>& objects,
		const std::vector<PointLight>& lights, float indexOfRefraction, int depth, glm::vec3& color, float& distance)
	{
		Ray::IntersectionType intersectionType;
		distance = FLT_MAX;

		int closestIntersectionIndex = -1;

		for (int i = 0; i < objects.size(); i++)
		{
			const std::shared_ptr<Object>& object = objects[i];

			Ray::IntersectionType tempType;
			if (tempType = object->intersect(ray, distance))
			{
				intersectionType = tempType;
				closestIntersectionIndex = i;
			}
		}

		if (closestIntersectionIndex >= 0)
		{
			const std::shared_ptr<Object>& objectHit = objects[closestIntersectionIndex];
			const Material material = objectHit->getMaterial();

			glm::vec3 intersectionPoint = ray.getOrigin() + distance * ray.getDirection();

			glm::vec3 normal = objectHit->getNormalAt(intersectionPoint);

			for (PointLight light : lights)
			{
				// Cast shadow rays
				float shadow = 1.0f;
				glm::vec3 lightDirection = light.getPosition() - intersectionPoint;
				
				float t2 = glm::length(lightDirection);

				lightDirection = glm::normalize(lightDirection);
				Ray shadowRay(intersectionPoint + EPSILON * lightDirection, lightDirection);
				for (const std::shared_ptr<Object>& object : objects)
				{
					if (object->intersect(shadowRay, t2) == Ray::IntersectionType::HIT)
					{
						shadow = 0;
						break;
					}
				}

				// Diffuse component
				if (shadow > 0)
				{

					if (material.getDiffuse() > 0)
					{
						float dot = glm::dot(lightDirection, normal);

						if (dot > 0)
						{
							float diffuseFactor = dot * material.getDiffuse() * shadow;
							color += diffuseFactor * material.getRGB() * light.getMaterial().getRGB();
						}
					}

					if (material.getSpecular() > 0)
					{
						// Specular component
						glm::vec3 reflectedLightDirection = glm::normalize(glm::reflect(lightDirection, normal));

						float dot = glm::dot(reflectedLightDirection, ray.getDirection());

						if (dot > 0)
						{
							float specular = powf(dot, 20) * material.getSpecular() * shadow;

							color += specular * material.getRGB();
						}
					}
				}
			}

			// Calculate reflections
			float reflectivity = material.getReflectivity();
			if (reflectivity > 0.0f)
			{
				glm::vec3 reflectedRayDirection = glm::normalize(glm::reflect(ray.getDirection(), normal));

				if (depth > 0)
				{
					float newDistance;
					glm::vec3 newColor;
					
					trace(Ray(intersectionPoint + reflectedRayDirection * EPSILON, reflectedRayDirection), objects, lights, indexOfRefraction, depth - 1, newColor, newDistance);

					color += reflectivity * newColor * material.getRGB();
				}
			}

			// Calculate refraction
			if (material.doesRefract())
			{
				float currentIndexOfRefraction = material.getIndexOfRefraction();

				float iorRatio = indexOfRefraction / currentIndexOfRefraction;

				normal *= (float)intersectionType;

				float cosIncident = -glm::dot(ray.getDirection(), normal);
				float cosRefractedSquared = 1.0f - (iorRatio * iorRatio) * (1.0f - cosIncident * cosIncident);

				// To avoid total internal reflection
				if (cosRefractedSquared > 0.0f)
				{
					if (depth > 0)
					{
						glm::vec3 refractionDirection = glm::refract(ray.getDirection(), normal, iorRatio);

						float newDistance;
						glm::vec3 newColor;

						trace(Ray(intersectionPoint + refractionDirection * EPSILON, refractionDirection), objects, lights, currentIndexOfRefraction, depth - 1, newColor, newDistance);

						// Apply Beer's law
						color += newColor * glm::exp(material.getRGB() * -newDistance);

					}
				}
			}
		}

		color = Math::clamp(color);
	}
}