#ifndef Material_H
#define Material_H

#include <math.h>
#include <glm\glm.hpp>

class Material
{
public:
	Material();

	Material(const glm::vec4& color, float diffuse = 1.0f, float specular = 0.0f, float reflectivity = 0.0f, bool refracts = false, float indexOfRefraction = 0.0f);

	Material(float r, float g, float b, float a, float diffuse = 1.0f, float specular = 0.0f, float reflectivity = 0.0f, bool refracts = false, float indexOfRefraction = 0.0f);

	inline const glm::vec3 getRGB() const { return glm::vec3(m_color); }
	
	inline const float& getDiffuse() const { return m_diffuse; }
	inline const float& getReflectivity() const { return m_reflectivity; }
	inline const float& getSpecular() const { return m_specular; }

	inline const bool& doesRefract() const { return m_refracts; }
	inline const float& getIndexOfRefraction() const { return m_indexOfRefraction; }

private:
	glm::vec4 m_color;

	// Material factors
	float m_diffuse;
	float m_specular;

	bool m_refracts;
	float m_indexOfRefraction;

	float m_reflectivity;
};
#endif
