#include "Material.h"

Material::Material() : m_color(glm::vec4(0.5, 0.5, 0.5, 0.0)), m_diffuse(0.65), m_specular(0), m_reflectivity(0), m_refracts(false), m_indexOfRefraction(0)
{

}

Material::Material(const glm::vec4& material, float diffuse, float specular, float reflectivity, bool refracts, float indexOfRefraction) : m_color(material), m_diffuse(diffuse), m_specular(specular), m_reflectivity(reflectivity), m_refracts(refracts), m_indexOfRefraction(indexOfRefraction)
{

}

Material::Material(float r, float g, float b, float a, float specular, float diffuse, float reflectivity, bool refracts, float indexOfRefraction) :
	m_color(glm::vec4(r, g, b, a)), m_specular(specular), m_diffuse(diffuse), m_refracts(refracts), m_indexOfRefraction(indexOfRefraction)
{

}