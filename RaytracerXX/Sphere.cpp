#include "Sphere.h"
#include "Math.h"

Sphere::Sphere()
{
	m_origin = glm::vec3();
	m_radius = 1;
	m_material = Material();
}

Sphere::Sphere(glm::vec3 origin, double radius, const Material& material)
{
	this->m_origin = origin;
	this->m_radius = radius;
	this->m_material = material;
}

Ray::IntersectionType Sphere::intersect(Ray ray, float& t)
{
	glm::vec3 v = ray.getOrigin() - m_origin;
	float b = -glm::dot(v, ray.getDirection());
	float det = (b * b) - glm::dot(v, v) + (m_radius * m_radius);
	Ray::IntersectionType retval = Ray::IntersectionType::MISS;
	if (det > 0)
	{
		det = sqrtf(det);
		float i1 = b - det;
		float i2 = b + det;
		if (i2 > 0)
		{
			if (i1 < 0)
			{
				if (i2 < t)
				{
					t = i2;
					retval = Ray::IntersectionType::INTERNAL;
				}
			}
			else
			{
				if (i1 < t)
				{
					t = i1;
					retval = Ray::IntersectionType::HIT;
				}
			}
		}
	}
	return retval;
}

glm::vec3 Sphere::getNormalAt(glm::vec3 point)
{
	return glm::normalize(point - m_origin);
}