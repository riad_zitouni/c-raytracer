#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H

#include "Material.h"
#include <math.h>

namespace rt
{
	class PointLight
	{
	public:
		PointLight();

		PointLight(glm::vec3 position, const Material& Material);

		glm::vec3 getPosition() { return m_position; }
		Material getMaterial() { return m_material; }

	private:
		glm::vec3 m_position;
		Material m_material;
	};
}
#endif
