// TODO: Clean
#include "Camera.h"
#include "Sphere.h"
#include "Plane.h"
#include "Math.h"
#include "Raytracer.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <limits>
#include <memory>
#include <string>

#include <windows.h>
#include <tchar.h>

#include <glm/gtc/type_ptr.hpp>

using namespace rt;

char bitmapbuffer[sizeof(BITMAPINFO) + 16];

static TCHAR windowClassName[] = _T("Raytracer");
static TCHAR windowTitle[] = _T("Raytracer");

int width = 640;
int height = 480;

COLORREF* imageBuffer;

//Handles events when it receives messages
LRESULT CALLBACK wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	switch (message)
	{
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		hdc = BeginPaint(hWnd, &ps);

		// Get size of window
		RECT rect;
		GetClientRect(hWnd, &rect); 

		// Create a device context in memory
		HDC hDCMem = CreateCompatibleDC(hdc); 
		int savedDC = SaveDC(hDCMem);

		COLORREF c = RGB(255, 0, 0);

		HBITMAP Membitmap = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
		SelectObject(hDCMem, Membitmap);

		// Now do your drawing to hDCMem here:
		for (int x = 0; x < width/*rect.right - rect.left*/; x++)
		{
			for (int y = 0; y < height/*rect.bottom - rect.top*/; y++)
			{
				SetPixelV(hDCMem, x, y, imageBuffer[x * height + y]);
			}
		}

		// Now BitBlt the stuff from memory buffer to screen
		BitBlt(hdc, 0, 0, rect.right, rect.bottom, hDCMem, 0, 0, SRCCOPY);

		// Cleanup time
		RestoreDC(hDCMem, savedDC);
		DeleteDC(hDCMem);
		DeleteDC(hdc);
		EndPaint(hWnd, &ps);
		break;
		
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		break;
	}
	case WM_CLOSE:
	{
		ReleaseDC(hWnd, GetDC(hWnd));
		DestroyWindow(hWnd);
		SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, 1, 0, 0);
		ExitProcess(0);
		break;
	}
	default:
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	}

	return 0;
}

bool initWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wc;

	// size of structs
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = wndProc;

	// Extra bytes allocted following structure
	wc.cbClsExtra = 0;
	
	// Number of extra bytes allocated following window instance
	wc.cbWndExtra = 0;
	
	// Instance that contains window procedure
	wc.hInstance = hInstance;

	// Class icon
	wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	// Cursor s
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);

	// Background brush/color
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);

	wc.lpszMenuName = NULL;

	// Window class name
	wc.lpszClassName = windowClassName;

	// Small icon
	wc.hIconSm = LoadIcon(wc.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	// Register window so that windows knows how to send messages to it
	if (!RegisterClassEx(&wc))
	{
		// Owner, message to display, dialog box title
		MessageBox(NULL, _T("Failed to register window class"), windowClassName, NULL);

		return false;
	}

	// Create window
	HWND hWnd = CreateWindow(
		windowClassName, // Name of application
		windowTitle, // Title that appears in window
		WS_OVERLAPPEDWINDOW, // Type of windows
		CW_USEDEFAULT, CW_USEDEFAULT,// Initial position (x, y) 
		width, height, // Initial size (width, height);
		NULL, // Parenf of window 
		NULL, // No menu bar
		hInstance, 
		NULL // ot used in this app
	);

	if (!hWnd)
	{
		MessageBox(NULL, _T("Failed to create window"), windowClassName, NULL);
		return false;
	}

	// Make window visible
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return true;
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	imageBuffer = new COLORREF[width * height];

	float fov = 45;
	float aspectRatio = float(width) / height;

	float tanHalfFov = tan(degreesToRadians(0.5f * fov));

	Camera camera(glm::vec3(-5.0f, 5.0f, 35.0f), glm::vec3(5.0f, 1.0f, 0.0f));

	std::vector<std::shared_ptr<Object>> objects;

	std::shared_ptr<Sphere> sphere = std::make_shared<Sphere>(glm::vec3(5.0,6, 15), 4, Material(glm::vec4(0.7f, 0.0f, 0.0f, 0.0f), 0.8f, 0.2f, 0.0f, true, 1.3f));
	std::shared_ptr<Sphere> sphere2 = std::make_shared<Sphere>(glm::vec3(-5.0f, 6, 0), 4, Material(glm::vec4(0.5f, 0.5f, 0.0f, 0.0f), 0.75f, 0.95f, 0.7f));

	std::shared_ptr<Plane> floorPlane = std::make_shared<Plane>(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), Material(glm::vec4(0.4f, 0.3f, 0.3f, 0.0f)));

	std::shared_ptr<Plane> backPlane = std::make_shared<Plane>(glm::vec3(0.0f, 0.0f, -50.0f), glm::vec3(0.0f, 0.0f, 1.0f), Material(glm::vec4(0.6f, 0.3f, 0.5f, 0.0f)));

	std::shared_ptr<Plane> topPlane = std::make_shared<Plane>(glm::vec3(0.0f, 20.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f), Material(glm::vec4(0.6f, 0.3f, 0.5f, 0.0f)));

	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 10; y++)
		{
			std::shared_ptr<Sphere> sphere = std::make_shared<Sphere>(glm::vec3(1 + x * 2.5f, 1 + y * 2.5f, -20), 0.7f, Material(glm::vec4(0.0f, 1.0f, 0.0f, 0.0f), 0.6f, 0.6f));
			objects.push_back(std::move(sphere));
		}
	}

	objects.push_back(sphere);
	objects.push_back(sphere2);
	objects.push_back(floorPlane);
	objects.push_back(backPlane);
	objects.push_back(topPlane);

	std::vector<PointLight> lights;
	PointLight light;

	lights.push_back(PointLight(glm::vec3(10.0f, 10.0f, 5.0f), Material(glm::vec4(0.6f, 0.6f, 0.8f, 0.0f), 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)));
	lights.push_back(PointLight(glm::vec3(-10.0f, 10.0f, 20.0f), Material(glm::vec4(0.4f, 0.4f, 0.4f, 0.0f), 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)));

	Raytracer raytracer;
	int depth = 6;
	int numberOfSteps = 2;
	int numberOfStepsSquared = (numberOfSteps + 1) * (numberOfSteps + 1);
	float step = 1.0f / numberOfSteps;

	for (int y = 0; y < height; y++)
	{
		OutputDebugString(L"Rendering");
		for (int x = 0; x < width; x++)
		{
			glm::vec3 finalColor;
			for (float dx = 0; dx <= 1; dx += step)
			{
				for (float dy = 0; dy <= 1; dy += step)
				{
					float cameraX = ((2 * (x + dx) / width) - 1) * aspectRatio * tanHalfFov;
					float cameraY = (1 - (2 * (y + dy) / height)) * tanHalfFov;

					glm::vec4 cameraRayDirection(cameraX, cameraY, -1, 0);
					glm::vec3 worldRayDirection = glm::normalize(glm::vec3(camera.getCameraToWorldMatrix() * cameraRayDirection));

					float t;
					glm::vec3 color;
					raytracer.trace(Ray(camera.getPosition(), worldRayDirection), objects, lights, 1.0f, depth, color, t);

					finalColor += color;
				}
			}
	
			finalColor /= numberOfStepsSquared;

			imageBuffer[x * height + y] = RGB(finalColor.x * 255, finalColor.y * 255, finalColor.z * 255);
		}
	}

	if (!initWindow(hInstance, nCmdShow))
	{
		return 1;
	}

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	delete imageBuffer;

	return (int)msg.wParam;
}

void saveToPPM()
{
	/*std::ofstream ofs("./render.ppm", std::ios::out | std::ios::binary);
	ofs << "P6\n" << width << " " << height << "\n255\n";
	for (unsigned i = 0; i < width * height; ++i)
	{
		ofs << (unsigned char)(image[i].x * 255) <<
			(unsigned char)(image[i].y * 255) <<
			(unsigned char)(image[i].z * 255);
	}
	ofs.close();
	delete[] image;*/
}
