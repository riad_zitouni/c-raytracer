#ifndef MATH_H
#define MATH_H

#include <cmath>
#include <algorithm>

#define M_PI 3.14159265358979

#define degreesToRadians(degress) (degress * M_PI / 180.0f)

// TODO: create namespace
namespace rt
{
	static class Math
	{
	public:
		static bool solveQuadratic(const float& a, const float& b, const float& c, float& x1, float& x2)
		{
			float delta = b * b - 4 * a * c;

			if (delta < 0)
			{
				return false;
			}
			else if (delta == 0)
			{
				x1 = x2 = (-0.5 * b) / a;
			}
			else
			{
				float sqrtDelta = sqrt(delta);

				x1 = (-0.5 * (b - sqrtDelta)) / a;
				x2 = (-0.5 * (b + sqrtDelta)) / a;
			}

			return true;
		}

		static glm::vec4 clamp(const glm::vec4& vector)
		{
			float x = std::max(0.0f, std::min(vector.x, 1.0f));
			float y = std::max(0.0f, std::min(vector.y, 1.0f));
			float z = std::max(0.0f, std::min(vector.z, 1.0f));
			float w = std::max(0.0f, std::min(vector.w, 1.0f));

			return glm::vec4(x, y, z, w);
		}

		static glm::vec3 clamp(const glm::vec3& vector)
		{
			float x = std::max(0.0f, std::min(vector.x, 1.0f));
			float y = std::max(0.0f, std::min(vector.y, 1.0f));
			float z = std::max(0.0f, std::min(vector.z, 1.0f));

			return glm::vec3(x, y, z);
		}
	};
}
#endif