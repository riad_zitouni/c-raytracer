#include "Ray.h"

Ray::Ray()
{
	m_origin = glm::vec3();
	m_direction = glm::vec3(1, 0, 0);
}

Ray::Ray(glm::vec3 origin, glm::vec3 direction)
{
	this->m_origin = origin;
	this->m_direction = direction;
}