#include "Camera.h"

#include <glm/gtc/type_ptr.hpp>

namespace rt
{
	Camera::Camera()
	{
		m_position = glm::vec3();
		m_direction = glm::vec3(0, 0, -1);
		m_right = glm::vec3(1, 0, 0);
		m_up = glm::vec3(0, 1, 0);
	}

	Camera::Camera(glm::vec3 position, glm::vec3 lookAt)
	{
		glm::vec3 direction = glm::normalize(lookAt - position);

		glm::vec3 right = glm::normalize(glm::cross(direction, WORLD_UP));

		glm::vec3 up = glm::normalize(glm::cross(right, direction));

		m_position = position;
		m_direction = direction;
		m_right = right;
		m_up = up;

		setCameraMatrix();
	}

	Camera::Camera(glm::vec3 campos, glm::vec3 camdir, glm::vec3 camright, glm::vec3 up)
	{
		m_position = campos;
		m_direction = camdir;
		m_right = camright;
		m_up = up;

		setCameraMatrix();
	}

	void Camera::setCameraMatrix()
	{
		float cameraArray[16] = {
			m_right.x, m_up.x, -m_direction.x, m_position.x,
			m_right.y, m_up.y, -m_direction.y, m_position.y,
			m_right.z, m_up.z, -m_direction.z, m_position.z,
			0        , 0     , 0             , 1	   
		};

		m_cameraToWorldMatrix = glm::inverse(glm::make_mat4(cameraArray));
	}

}