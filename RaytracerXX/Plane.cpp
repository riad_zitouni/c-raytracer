#include "Plane.h"

namespace rt
{
	Plane::Plane()
	{
		normal = glm::vec3(0, 0, -1);
		origin = glm::vec3(0);
		material = Material();
	}

	Plane::Plane(glm::vec3 origin, glm::vec3 normal, const Material& material)
	{
		this->origin = origin;
		this->normal = glm::normalize(normal);
		this->material = material;
	}

	Ray::IntersectionType Plane::intersect(Ray ray, float& t)
	{
		float denominator = glm::dot(normal, ray.getDirection());
		if (denominator != 0)
		{
			float distance = -(glm::dot(normal, ray.getOrigin()) + glm::length(origin)) / denominator;
			if (distance > 0)
			{
				if (distance < t)
				{
					t = distance;
					return Ray::IntersectionType::HIT;
				}
			}
		}
		return Ray::IntersectionType::MISS;
	}

	glm::vec3 Plane::getNormalAt(glm::vec3 point)
	{
		return normal;
	}
}